import { name, random, date, lorem } from "faker/locale/id_ID";
import { createObjectCsvWriter } from "csv-writer";

const scale = 1;

const pengguna = Array.from(Array(1_000 * scale), (_) => ({
  id: random.uuid(),
  nama_depan: name.firstName(),
  nama_belakang: name.lastName(),
}));

createObjectCsvWriter({
  path: "pengguna.csv",
  header: [
    { id: "id", title: "id_pengguna" },
    { id: "nama_depan", title: "nama_depan" },
    { id: "nama_belakang", title: "nama_belakang" },
  ],
}).writeRecords(pengguna);

///

const investasi = Array.from(Array(5_000 * scale), (_) => ({
  id: random.uuid(),
  id_pengguna: random.arrayElement(pengguna).id,
  besar_investasi: random.float({ min: 0.1, max: 1_000_000 }),
  tanggal: date.past(),
}));

createObjectCsvWriter({
  path: "investasi.csv",
  header: [
    { id: "id", title: "id_investasi" },
    { id: "id_pengguna", title: "id_pengguna" },
    { id: "besar_investasi", title: "besar_investasi" },
    { id: "tanggal", title: "tanggal_investasi" },
  ],
}).writeRecords(investasi);

///

const penarikan = Array.from(Array(2_000 * scale), (_) => ({
  id: random.uuid(),
  id_pengguna: random.arrayElement(pengguna).id,
  besar_penarikan: random.float({ min: 0.1, max: 1_000_000 }),
  tanggal: date.past(),
}));

createObjectCsvWriter({
  path: "penarikan.csv",
  header: [
    { id: "id", title: "id_investasi" },
    { id: "id_pengguna", title: "id_pengguna" },
    { id: "besar_penarikan", title: "besar_penarikan" },
    { id: "tanggal", title: "tanggal_penarikan" },
  ],
}).writeRecords(penarikan);

///

const saran = Array.from(Array(1000 * scale), (_) => {
  const inv = random.float({ min: 0.1, max: 100_000 });
  return {
    id: random.uuid(),
    id_pengguna: random.arrayElement(pengguna).id,
    besar_investasi: inv,
    besar_target: random.float({ min: inv, max: inv * 10 }),
    lama_investasi: random.number({ min: 1, max: 24 }),
    tanggal: date.past(),
  };
});

createObjectCsvWriter({
  path: "saran.csv",
  header: [
    { id: "id", title: "id_saran" },
    { id: "id_pengguna", title: "id_pengguna" },
    { id: "besar_investasi", title: "besar_investasi_saran" },
    { id: "besar_target", title: "besar_target_saran" },
    { id: "lama_investasi", title: "lama_investasi_saran" },
    { id: "tanggal", title: "tanggal_saran" },
  ],
}).writeRecords(saran);

///

const feedback = saran
  .map((s) => {
    if (random.boolean()) {
      return {};
    }
    return {
      id: random.uuid(),
      id_saran: s.id,
      id_pengguna: s.id_pengguna,
      rating: random.float({ min: 1.0, max: 5.0 }),
      isi_feedback: lorem.sentence(),
      tanggal: date.future(1, s.tanggal),
    };
  })
  .filter((f) => Object.keys(f).length);

createObjectCsvWriter({
  path: "feedback.csv",
  header: [
    { id: "id", title: "id_feedback" },
    { id: "id_saran", title: "id_saran" },
    { id: "id_pengguna", title: "id_pengguna" },
    { id: "rating", title: "rating" },
    { id: "isi_feedback", title: "isi_feedback" },
    { id: "tanggal", title: "tanggal_feedback" },
  ],
}).writeRecords(feedback);
